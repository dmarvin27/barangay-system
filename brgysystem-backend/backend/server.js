const http = require('http');
const app = require('./app')

const port = process.env.PORT || 3000;

const server = http.createServer(app);

server.listen(port, function () {
    console.log("============================================");
    console.log("Started listening at 'http://localhost:" + port + "'");
    console.log("============================================");
});

exports.httpMsgsFormat = "HTML"