const constituentModel = require('../models/constituentModel');
const httpMsgs = require('../controllers/httpMsg');

  	exports.getConstituent = function (req, res) {
    	constituentModel.getConstituent(req, res);
  	};

	exports.addConstituent = function (req, res) {
		constituentModel.addConstituent(req, res);
	};

	exports.deleteConstituent = function (req, res) {
		constituentModel.deleteConstituent(req, res);
	};

	exports.updateConstituent = function (req, res) {
		constituentModel.updateConstituent(req, res);
	};

	exports.getConstituentConstituentByID = function (req, res) {
		constituentModel.getConstituentConstituentByID(req, res);
	};