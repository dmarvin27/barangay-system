const HTML = require('/Users/Justin Lyeo Jaring/Documents/Project/expresShit/backend/server');

exports.get404 = (req, res, err) => {
    if (HTML.httpMsgsFormat === "HTML") {
        res.writeHead(404,"Resource not found", { "Content-Type" : "text/html" });
        res.write("<html><head><title>404</title></head><body>404: Resource not found. Details:"+ err +"</body></html>");
    }
    else {
        res.writeHead(404, "Resource not found", { "Content-Type" : "application/json" });
        res.write(JSON.stringify({ data: "ERROR occured:" + err }));
    }
};

exports.get405 = (req, res, err) => {
    if (HTML.httpMsgsFormat === "HTML") {
        res.writeHead(405,"Method not supported", { "Content-Type" : "text/html" });
        res.write("<html><head><title>405</title></head><body>405: Method not supported. Details:"+ err +"</body></html>");
    }
    else {
        res.writeHead(405, "Method not supported", { "Content-Type" : "application/json" });
        res.write(JSON.stringify({ data: "ERROR occured:" + err }));
    }
};

exports.get413 = (req, res, err) => {
    if (HTML.httpMsgsFormat === "HTML") {
        res.writeHead(413,"Request Entity too large", { "Content-Type" : "text/html" });
        res.write("<html><head><title>413</title></head><body>413: Request Entity too large. Details:"+ err +"</body></html>");
    }
    else {
        res.writeHead(413, "Request Entity too large", { "Content-Type" : "application/json" });        
        res.write(JSON.stringify({ data: "ERROR occured:" + err }));
    }
};

exports.get500 = (req, res, err) => {
    if (HTML.httpMsgsFormat === "HTML") {
        res.writeHead(500,"Internal Error occured", { "Content-Type" : "text/html" });
        res.write("<html><head><title>500</title></head><body>500: Internal Error. Details:"+ err +"</body></html>");
    }
    else {
        res.writeHead(500, "Internal Error occured", { "Content-Type" : "application/json" })
        res.write(JSON.stringify({ data: "ERROR occured:" + err }));
    }
};

exports.get200 = (req, res, err) => {
    res.writeHead(200, { "Content-Type" : "application/json" });
    res.end();
};