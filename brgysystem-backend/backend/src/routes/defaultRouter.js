const express = require('express');
const router = express.Router();

router.get('/', (req, res, next) => {
    res.send('Namba 1 Mutha Fucker');
});

router.post('/', (req, res, next) => {
    const data = {
        postID: req.body.ID,
        postName: req.body.name
    };
    res.status(201).json({
        message: 'Post Order',
        data: data
    });
});

router.patch('/:patchID', (req, res, next) => {
    res.status(200).json({
        message: 'Patch Success',
        id: req.params.patchID
    });
});

router.delete('/:deletedID', (req, res, next) => {
    res.status(200).json({
        message: 'Delete Success',
        id: req.params.deletedID
    });
});

module.exports = router;