const express = require('express');
const router = express.Router();

const constituentController = require('../controllers/constituentController');

router.get('/constituent', constituentController.getConstituent);

router.post('/add-constituent', constituentController.addConstituent);

router.delete('/delete-constituent/:Id', constituentController.deleteConstituent);

router.patch('/update-constituent/:Id', constituentController.updateConstituent);

router.get('/get-constituent-byID/:Id', constituentController.getConstituentConstituentByID);

module.exports = router;
