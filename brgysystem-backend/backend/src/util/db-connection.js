const mssql = require("mssql");
const config = require('./db-config');

exports.executeSql = function (sql, callback) {
    var conn = new mssql.ConnectionPool(config.dbConfig);
    conn.connect().then(function () {
        var req = new mssql.Request(conn);
        req.query(sql).then(function (recordset) {
            callback(recordset);
        }).catch(function (err) {
            console.log(err);
            callback(null,err);
        })
    }).catch(function (err) {
        console.log(err);
        callback(null,err);
    });
}