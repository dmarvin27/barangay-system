const db = require('../util/db-connection');
const httpMsgs = require('../controllers/httpMsg');

module.exports = class ConstituentModel {

    static getConstituent(req, res) {
        try {
			return db.executeSql('SELECT * FROM ConstituentInfo',function(data, err) {
				if (err) {
					httpMsgs.show500(req, res, err);
				}
				else {
					res.send(data.recordset);
					// res.status(200).json({  message: 'Get Constituent'  });
				}
			});
        } catch(exception) {
            res.status(500).send(exception)
		}
    } // end getConstituent

    static addConstituent(req, res) {
        try {
			var query = "INSERT INTO ConstituentInfo \
			(FirstName, MiddleName, LastName, Suffix, MothersName, FathersName, \
			Gender, CivilStatus, Birthday, Birthplace, PhoneNumber, Citizenship, Religion, Email) \
			VALUES \
			('" + req.body.FirstName + "' \
			,'" + req.body.MiddleName + "' \
			,'" + req.body.LastName + "' \
			,'" + req.body.Suffix + "' \
			,'" + req.body.MothersName + "' \
			,'" + req.body.FathersName + "' \
			,'" + req.body.Gender + "' \
			,'" + req.body.CivilStatus + "' \
			,'" + req.body.Birthday + "' \
			,'" + req.body.Birthplace + "' \
			,'" + req.body.PhoneNumber + "' \
			,'" + req.body.Citizenship + "' \
			,'" + req.body.Religion + "' \
			,'" + req.body.Email + "' \
			)";

			return db.executeSql(query, function (data, err) {
				if (err) {
					httpMsgs.show500(req, res, err);
				}
				else {
					// httpMsgs.send200(req, res);
					res.status(200).json({ message: 'Post Success', id: req.params.Id });
				}
			});
        } catch(exception) {
            res.status(500).send(exception)
		}
    } // end addConstituent

	static deleteConstituent(req, res) {
		try {
			var query = "DELETE FROM ConstituentInfo WHERE Id = '" + req.params.Id + "'";
				
			return db.executeSql(query, function (data, err) {
				if (err) {
					httpMsgs.show500(req, res, err);
				}
				else {
					// httpMsgs.send200(req, res);
					res.status(200).json({ message: 'Deleted Success', id: req.params.Id });
				}
			});
		} catch (exception) {
			res.status(500).send(exception)
		}
	} // end deleteConstituent

	static updateConstituent(req, res) {
		try {
			var query = "UPDATE ConstituentInfo set \
				[FirstName] = '" + req.body.FirstName + "'\
				,[MiddleName] = '" + req.body.MiddleName + "'\
				,[LastName] = '" + req.body.LastName + "'\
				,[Suffix] = '" + req.body.FirstName + "'\
				,[MothersName] ='" + req.body.MothersName + "' \
				,[FathersName] = '" + req.body.FathersName + "' \
				,[Gender] = '" + req.body.Gender + "' \
				,[CivilStatus] = '" + req.body.CivilStatus + "' \
				,[Birthday] = '" + req.body.Birthday + "' \
				,[Birthplace] = '" + req.body.Birthplace + "' \
				,[PhoneNumber] = '" + req.body.PhoneNumber + "' \
				,[Citizenship] = '" + req.body.Citizenship + "' \
				,[Religion] = '" + req.body.Religion + "' \
				,[Email] = '" + req.body.Email + "' \
				where ID = '" + req.params.Id + "'";

			return db.executeSql(query, function (data, err) {
				if (err) {
					httpMsgs.show500(req, res, err);
				}
				else {
					// httpMsgs.send200(req, res);
					res.status(200).json({ message: 'Update Success', id: req.params.Id });
				}
			});
		} catch (exception) {
			res.status(500).send(exception)
		}
	} // end updateConstituent

	static getConstituentConstituentByID(req, res) {
		try {
			var query = "SELECT * FROM ConstituentInfo where ID = '" + req.params.Id + "'";

			return db.executeSql(query, function (data, err) {
				if (err) {
					httpMsgs.show500(req, res, err);
				}
				else {
					res.send(data.recordset);
				}
			});
		} catch (exception) {
			res.status(500).send(exception)
		}
	} // end getConstituentConstituentByID

}; // end Class