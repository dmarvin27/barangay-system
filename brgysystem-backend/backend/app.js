const express = require("express");
const app = express();
const bodyParser = require("body-parser");

const errorController = require('./src/controllers/httpMsg');
const defaultRoute = require('./src/routes/defaultRouter');
const constituentRoutes = require('./src/routes/constituentRoutes');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// C-O-R-S
app.use((req, res, next) => {
     
    // Website you wish to allow to connect
    res.header("Access-Control-Allow-Origin", "*");

    // Request headers you wish to allow
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

    // Request methods you wish to allow
    // res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    if (req.method === "OPTIONS") {
      res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
      return res.status(200).json({});
    }
    next();
});


// Routes that handle request
app.use(defaultRoute);
app.use(constituentRoutes);
app.use(errorController.get404);

module.exports = app;