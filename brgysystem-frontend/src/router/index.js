import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Dashboard from '@/components/Dashboard'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/hello',
      name: 'HelloWorld',
      component: HelloWorld
      //component:load('src/HelloWorld')
    }
    // {
    //   path: '/dashboard',
    //   name: 'dashboard',
    //   //component:load('src/Dashboard')
    //   component: Dashboard
    // }
  ]
})
